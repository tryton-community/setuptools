;; Per-directory local variables for GNU Emacs 23 and later.

((nil
  . ((fill-column . 78)
     (indent-tabs-mode . nil)  ;; insert spaces instead of tabs
     (sentence-end-double-space . f)
     (ispell-local-dictionary . "en_US")
     (safe-local-variable-values
      (eval add-hook 'prog-mode-hook #'flyspell-prog-mode)
      (flyspell-issue-message-flag . f)) ; avoid messages for every word
     )))
