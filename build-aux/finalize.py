"""
Finalize the repo for a release. Invokes towncrier and bumpversion.
"""
# Taken from setuptools.
# Author: Jason R. Coombs <jaraco@jaraco.com>
# The original file doesn't contain license information, thus
# I assume it the the same as setuptools, which is MIT License.
# SPDX-License-Identifier: MIT License

__requires__ = ['bump2version', 'towncrier', 'jaraco.develop>=7.21']


import pathlib
import re
import subprocess
import sys

from jaraco.develop import towncrier

release_kind = towncrier.release_kind()

bump_version_command = [
    sys.executable,
    '-m',
    'bumpversion',
]


def get_new_version():
    cmd = bump_version_command + ['show-bump', '--ascii']
    out = subprocess.check_output(cmd, text=True)
    parts = out.strip().split()
    try:
        i = parts.index(release_kind)
        return parts[i + 2]
    except ValueError:
        raise SystemExit(
            f"Couldn't find {release_kind!r} in output of bumpversion")
    except IndexError:
        raise SystemExit("Couldn't parse output of bumpversion")


def update_changelog(new_version):
    cmd = (
        sys.executable,
        '-m',
        'towncrier',
        'build', '--yes',
        '--version',
        f'v{new_version}',
    )
    subprocess.check_call(cmd)
    _repair_changelog()


def _repair_changelog():
    """
    Workaround for #2666
    """
    changelog_fn = pathlib.Path('NEWS.rst')
    changelog = changelog_fn.read_text(encoding='utf-8')
    fixed = re.sub(r'^(v[0-9.]+)v[0-9.]+$', r'\1', changelog, flags=re.M)
    changelog_fn.write_text(fixed, encoding='utf-8')
    subprocess.check_output(['git', 'add', changelog_fn])


def bump_version():
    cmd = bump_version_command + ['bump', release_kind, '--allow-dirty']
    subprocess.check_call(cmd)


def ensure_config():
    """
    Double-check that Git has an e-mail configured.
    """
    subprocess.check_output(['git', 'config', 'user.email'])


if __name__ == '__main__':
    new_version = get_new_version()
    print("Cutting release at", new_version)
    ensure_config()
    towncrier.check_changes()
    update_changelog(new_version)
    bump_version()
