workflow:
  rules:
    # only run pipelines for merge requests, tags and protected branches
    # source:
    # https://docs.gitlab.com/ee/ci/yaml/workflow.html#git-flow-with-merge-request-pipelines
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"  # created or updated
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_REF_PROTECTED == "true"

stages:
  - test
  - build
  - deploy

.check:
  stage: test
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim

.check-doc:
  extends: .check
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/sphinxdoc/sphinx
  variables:
    DOC_DIR: doc
    README_FILE: README.rst
  rules:
    # only run if documentation changed
    - changes:
        - ${DOC_DIR}/**/*
        - CHANGES     # gets included
      when: on_success
    - when: never
  cache:
    paths:
      - .cache/pip
  before_script:
    - test -r "${DOC_DIR}/requirements-doc.txt" 
      && pip install -r "${DOC_DIR}/requirements-doc.txt"
    - pip install sphinx-lint rstcheck[sphinx]
  script:
    - python -m sphinx -T -E -W -n -b html ${DOC_DIR} _build/html
    - python -m sphinxlint ${DOC_DIR}
    - rstcheck --recursive ${DOC_DIR}

check-doc-readme:
  extends: .check-doc
  script:
    - python -m sphinxlint ${README_FILE}
    - rstcheck ${README_FILE}
  rules:
    # only run if README changed
    - changes:
        - ${README_FILE}
      when: on_success
    - when: never

check-flake8:
  extends: .check
  before_script:
    - pip install flake8
  script:
    - flake8

check-isort:
  extends: .check
  before_script:
    - pip install isort
  script:
    - isort --multi-line VERTICAL_GRID --check --diff .

.test-tox:
  stage: test
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:${PYTHON_VERSION}-slim
  parallel:
    matrix:
      - PYTHON_VERSION: ["3.6", "3.7", "3.8", "3.9", "3.10", "3.11", "3.12"]
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  cache:
    paths:
      - .cache/pip
  before_script:
    - python -m pip install tox
  script:
    - tox -e "py${PYTHON_VERSION/./}" -vv -- -v --output-file junit.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      junit: junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

build:
  stage: build
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  before_script:
    - pip install build twine
  script:
    - python -m build
    - python -m twine check --strict dist/*
  artifacts:
    paths:
      - dist

deploy-pypi:
  stage: deploy
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/python:slim
  rules:
    - if: $DEPLOY_TO_PYPI == null || $DEPLOY_TO_PYPI =~ /^(0|no|)$/i
      when: never
    # deploy only tags
    - if: $CI_COMMIT_TAG
      when: on_success
    - when: never
  before_script:
    - pip install twine
  script:
    - TWINE_PASSWORD=${PYPI_API_TOKEN} TWINE_USERNAME=__token__
      python -m twine upload dist/*
