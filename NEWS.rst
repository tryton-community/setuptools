v0.4.0 (2024-04-30)
===================

Features
--------

- Added support for Tryton 7.2. (#6)


Deprecations and Removals
-------------------------

- Dashes are not longer allowed in Tryton module name passed to
  ``get_prefix_require_version()``. (#5)
